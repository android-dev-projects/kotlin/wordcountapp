package com.example.instabugtask.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.instabugtask.databinding.WordCountCardBinding
import com.example.instabugtask.models.WordCount
import com.example.instabugtask.utils.WordsDiffUtil

class WordCountAdapter() : ListAdapter<WordCount, WordCountAdapter.WordsViewHolder>(WordsDiffUtil()) {

    class WordsViewHolder(private val cardBinding: WordCountCardBinding): RecyclerView.ViewHolder(cardBinding.root){
        companion object{
            fun from(parent: ViewGroup): WordsViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = WordCountCardBinding.inflate(layoutInflater, parent, false)
                return WordsViewHolder(binding)
            }
        }

        fun bind(word: WordCount){
            cardBinding.word = word
            cardBinding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordsViewHolder {
        return WordsViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: WordsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}