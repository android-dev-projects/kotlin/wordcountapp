package com.example.instabugtask.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.instabugtask.database.dao.WordsDao
import com.example.instabugtask.models.Word

@Database(entities = [Word::class], version = 1)
abstract class WordsDatabase: RoomDatabase() {
    abstract val wordsDao: WordsDao

    companion object{
        private lateinit var instance: WordsDatabase

        fun getInstance(context: Context): WordsDatabase {
            synchronized(this){
                if(!Companion::instance.isInitialized){
                    instance = Room.databaseBuilder(
                                context.applicationContext,
                                WordsDatabase::class.java,
                                "words_count_database"
                                )
                                .build()
                }
            }
            return instance
        }
    }
}