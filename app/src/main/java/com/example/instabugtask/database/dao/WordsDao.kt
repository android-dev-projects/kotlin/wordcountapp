package com.example.instabugtask.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.instabugtask.models.Word
import com.example.instabugtask.models.WordCount

@Dao
interface WordsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAllWords(words: List<Word>)

    @Query("select word, count(word) as wordcount from words_table where word like :wildCard group by word order by case when :sort = 1 then wordcount end asc, case when :sort = 0 then wordcount end desc , word asc")
    fun getAllWords(sort: Boolean = true, wildCard: String = "%"): LiveData<List<WordCount>>

    @Query("delete from words_table")
    fun deleteAll()

    @Query("delete from words_table where word = '' ")
    fun cleanup()
}