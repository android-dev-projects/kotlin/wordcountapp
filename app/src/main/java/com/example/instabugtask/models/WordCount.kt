package com.example.instabugtask.models

data class WordCount(
    var word: String,
    var wordcount: String
)