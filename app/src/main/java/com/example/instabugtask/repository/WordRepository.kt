package com.example.instabugtask.repository

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.instabugtask.database.WordsDatabase
import com.example.instabugtask.models.Word
import com.example.instabugtask.services.ApiClient
import com.example.instabugtask.ui.HomeFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class WordRepository(private val app:Application) {
    //To enforce a singleton object
    companion object{
        private lateinit var repoInstance: WordRepository

        fun getRepository(app: Application): WordRepository{
            synchronized(this){
                if(!::repoInstance.isInitialized){
                    repoInstance = WordRepository(app)
                }
                return repoInstance
            }
        }
    }

    private val wordsDatabase = WordsDatabase.getInstance(app.applicationContext).wordsDao

    fun getWords(sort: Boolean = true, wildcard: String = "%") = wordsDatabase.getAllWords(sort, wildcard)


    suspend fun GetData(){
        withContext(Dispatchers.IO) {
            try {
                wordsDatabase.deleteAll()
                var api = ApiClient.apiService.getIndexPage()
                var body = api.body()?.string()
                var collection = prepareData(body!!)

                val words: MutableList<Word> = mutableListOf()

                for (s in collection) {
                    words.add(Word(0,s))
                }

                wordsDatabase.addAllWords(words)
                wordsDatabase.cleanup()

            } catch (e: Exception) {
                e.message?.let { Log.d("WordsViewModel", "ERR " + it) }

                withContext(Dispatchers.Main){
                    Toast.makeText(app.baseContext, "Failed to Fetch Data, Please try again!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun prepareData(requestBody: String): MutableList<String>{
        var split = requestBody?.split(Regex("<body>"))
        var body = "<body>\n" + (split?.get(1))

        body = body.replace(Regex("<[^>]*>"), " ")
            .replace(Regex("@.*\\(.*\\)\\{.*>.*>.*>.*>.*\\..*\\{.*\\}\\}")," ").replace(Regex("\\..*-.*\\}")," ").trim()

        var list = body.split(" ").toMutableList()
        var collection = ""
        list.forEach{
            if(it != "" ) {
                collection += it.trimStart().trim() + "\n"
            }
        }

        collection = collection
            .replace(".", "")
            .replace("!", "")
            .replace(",", "")

        return collection.split("\n").toMutableList()
    }

}