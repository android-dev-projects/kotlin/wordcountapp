package com.example.instabugtask.services

import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.GET


interface ApiServiceInterface {
    @GET("/")
    suspend fun getIndexPage(): Response<ResponseBody>
}

object ApiClient {
    var apiService: ApiServiceInterface
    init{
        val retrofit = Retrofit.Builder()
            .baseUrl("https://instabug.com/")
            .build()

        apiService = retrofit.create(ApiServiceInterface::class.java)
    }
}