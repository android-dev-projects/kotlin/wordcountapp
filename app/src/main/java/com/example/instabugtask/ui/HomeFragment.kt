package com.example.instabugtask.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.instabugtask.R
import com.example.instabugtask.adapters.WordCountAdapter
import com.example.instabugtask.databinding.FragmentHomeBinding
import com.example.instabugtask.viewmodel.ViewModelFactory
import com.example.instabugtask.viewmodel.WordsViewModel


class HomeFragment : Fragment() {
    lateinit var binding: FragmentHomeBinding
    lateinit var viewModel: WordsViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (requireActivity() as AppCompatActivity).supportActionBar?.show()

        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(layoutInflater)

        val app = requireNotNull(this.activity).application

        val viewModelFactory = ViewModelFactory(app)

        viewModel = ViewModelProvider(this, viewModelFactory).get(WordsViewModel::class.java)

        binding.lifecycleOwner = this

        viewModel.getData()

        binding.rvCount.layoutManager = LinearLayoutManager(requireContext())
        binding.rvCount.adapter = WordCountAdapter()

        viewModel.words.observe(viewLifecycleOwner, Observer{
            it?.let{
                val adapter = binding.rvCount.adapter as WordCountAdapter
                adapter.submitList(it)
            }
        })

        viewModel.status.observe(viewLifecycleOwner, {
            binding.status = it
        })

        viewModel.search.observe(viewLifecycleOwner, {
            binding.search = it
        })

        viewModel.searchResult.observe(viewLifecycleOwner, Observer{
            it?.let{
                val adapter = binding.rvCount.adapter as WordCountAdapter
                adapter.submitList(it)
            }
        })

        viewModel.search.observe(viewLifecycleOwner, {
            it?.let{
                if(!it)
                    binding.searchBox.setText("")
            }
        })

        var watcher = object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int){}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var str = ""
                s?.forEach {
                    str += it
                }
                viewModel.find(str)
            }

            override fun afterTextChanged(s: Editable?) {}

        }

        binding.searchBox.addTextChangedListener(watcher)

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.action_bar_options, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.action_refresh -> {
                viewModel.getData()
                true
            }
            R.id.action_search -> {
                viewModel.doSearch()
                true
            }
            R.id.sort_asc -> {
                viewModel.enableSort()
                viewModel.getData()
                true
            }
            R.id.sort_desc -> {
                viewModel.disableSort()
                viewModel.getData()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}