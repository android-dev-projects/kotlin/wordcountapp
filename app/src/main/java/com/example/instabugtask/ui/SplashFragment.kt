package com.example.instabugtask.ui

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.example.instabugtask.R
import com.example.instabugtask.databinding.FragmentSplashBinding

class SplashFragment : Fragment() {
   override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

       (requireActivity() as AppCompatActivity).supportActionBar?.hide()

       // Inflate the layout for this fragment
       var binding = FragmentSplashBinding.inflate(layoutInflater)
       var handler = Handler()

       var runnable = Runnable{
           findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToHomeFragment())
       }

       handler.postDelayed(runnable, 1500)



        return binding.root
    }
}