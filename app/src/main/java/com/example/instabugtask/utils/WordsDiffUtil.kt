package com.example.instabugtask.utils

import androidx.recyclerview.widget.DiffUtil
import com.example.instabugtask.models.WordCount

class WordsDiffUtil: DiffUtil.ItemCallback<WordCount>() {
    override fun areItemsTheSame(oldItem: WordCount, newItem: WordCount): Boolean {
        return oldItem.word == newItem.word
    }

    override fun areContentsTheSame(oldItem: WordCount, newItem: WordCount): Boolean {
        return oldItem == newItem
    }

}