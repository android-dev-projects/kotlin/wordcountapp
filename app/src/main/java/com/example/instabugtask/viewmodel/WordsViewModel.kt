package com.example.instabugtask.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.example.instabugtask.repository.WordRepository
import kotlinx.coroutines.*

class WordsViewModel(private val app: Application): AndroidViewModel(app) {

    private val repo = WordRepository.getRepository(app)

    private val job = Job()

    private val scope = CoroutineScope(job + Dispatchers.IO)

    private var _search = MutableLiveData<Boolean>()

    val search: LiveData<Boolean>
        get() = _search

    private var _sort = MutableLiveData<Boolean>()

    val sort: LiveData<Boolean>
        get() = _sort

    private var _status = MutableLiveData<Boolean>()

    private var _wildCard = MutableLiveData<String>()

    val status: LiveData<Boolean>
        get() = _status

    init {
        _sort.value = true
        disableSort()
    }

    val words = Transformations.switchMap(sort){
        it?.let{
            liveData { emitSource(repo.getWords(it)) }
        }
    }

    val searchResult = Transformations.switchMap(_wildCard){
        it?.let{
            liveData { emitSource(repo.getWords(wildcard = it)) }
        }
    }

    fun getData(){
        _search.value = false
        scope.launch {
            _status.postValue(true)

            repo.GetData()

            _status.postValue(false)
            }
        }

    fun enableSort(){
        _sort.postValue(true)
    }

    fun disableSort(){
        _sort.postValue(false)
    }

    fun beginSearching(){
        _search.postValue(true)
    }

    fun endSearching(){
        _search.postValue(false)
    }

    fun doSearch(){
        if(search.value == true){
            endSearching()
        }
        else
            beginSearching()
    }

    fun find(string: String){
        _wildCard.value = "$string%"
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
 }